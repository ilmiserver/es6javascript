//  SOAL 1
// Normal Javascript
const golden = function goldenFunction(){
    console.log("this is golden!!")
  }   

golden()

//Jawaban Soal 1 ES6 fungsi arrow
const es6golden = () => console.log("this is golden!!")

es6golden()


// SOAL 2
// Normal Javascript
const newFunction = function literal(firstName, lastName){
  return {
    firstName: firstName,
    lastName: lastName,
    fullName: function(){
      console.log(firstName + " " + lastName)
      return 
    }
  }
}

newFunction("William", "Imoh").fullName()

//jawaban Soal 2 ES6 Object literal
const newes6 = function literal(firstName, lastName){
  return {
    firstName,
    lastName,
    fullName: () => console.log(firstName + " " + lastName)
  }
}

newes6("William", "Imoh").fullName()

// SOAL 3
// Jawaban Soal 3 ES6 Destructuring
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}

const { firstName, lastName, destination, occupation, spell } = newObject;
console.log(firstName, lastName, destination, occupation)


// Jawaban Soal 4 ES6 Array Spreading
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]

const combined = [...west, ...east]
console.log(combined)

// Jawaban Soal 5 ES6 Template Literals
const planet = "earth" 
const view = "glass" 
var before = `Lorem ${view} dolor sit amet consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`  
console.log(before) 



